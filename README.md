# Uptime-kuma-test

## Create docker-compose.yml
```bash
version: '3.3'

services:
  uptime-kuma:
    image: louislam/uptime-kuma
    container_name: uptime-kuma
    volumes:
      - ./uptime-kuma:/app/data
    ports:
      - 3001:3001
```

## Run Docker
With docker-compose file:
```bash
 docker-compose up -d
```
Or without docker-compose file:
```bash
 docker run -d --restart=always -p 3001:3001 -v uptime-kuma:/app/data --name uptime-kuma louislam/uptime-kuma:1
```

Go to : http://127.0.0.1:3001

